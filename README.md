# M4207

Répertoire où sera stockés tous les fichiers relatifs aux TD et TP.

Résultat test gps TD1 :

LGPS Power on, and waiting ...
LGPS loop
$GPGGA,235948.000,8960.0000,N,00000.0000,E,0,0,,137.0,M,13.0,M,,*42

UTC timer 23-59-48
latitude =  8960.0000, longitude =     0.0000
satellites number = 0
LGPS loop
$GPGGA,235950.000,8960.0000,N,00000.0000,E,0,0,,137.0,M,13.0,M,,*4B

UTC timer 23-59-50
latitude =  8960.0000, longitude =     0.0000
satellites number = 0
LGPS loop
$GPGGA,235952.000,8960.0000,N,00000.0000,E,0,0,,137.0,M,13.0,M,,*49

...

Résultat test serveur web :

Connecting to AP
SSID: ASUS_X008D
IP Address: 192.168.43.248
subnet mask: 255.255.255.0
gateway IP: 192.168.43.1
signal strength (RSSI):-22 dBm
Start Server
Server Started

Résultat test serveur web "Hello world" :

Connecting to AP
SSID: ASUS_X008D
IP Address: 192.168.43.248
subnet mask: 255.255.255.0
gateway IP: 192.168.43.1
signal strength (RSSI):-31 dBm
Start Server
Server Started
new client
GET / HTTP/1.1
Host: 192.168.43.248
Connection: keep-alive
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8
Accept-Encoding: gzip, deflate
Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7

send response
close connection
client disconnected
new client
GET /favicon.ico HTTP/1.1
Host: 192.168.43.248
Connection: keep-alive
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36
Accept: image/webp,image/apng,image/*,*/*;q=0.8
Referer: http://192.168.43.248/
Accept-Encoding: gzip, deflate
Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7

send response
close connection
client disconnected
new client

...

Test enregistrement de mémoire :

Initializing Storage card...Writing to test.txt...done.
test.txt:
testing 1, 2, 3.
