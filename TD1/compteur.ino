//Definition de la variable compteur

int compteur=0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("C'est parti !");
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.print("La valeur du compteur est ");
  Serial.println(compteur);
  compteur++;
  delay(250);
}
